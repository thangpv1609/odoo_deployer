{
    'name': 'Notify',
    'summary': 'Tính năng gửi thông báo',
    'version': '15.0.0.0',
    'website': 'https://2nf.com.vn/',
    'depends': [
        "web",
        "bus",
        "base",
        'mail'
    ],
    'data': [
        'security/ir.model.access.csv',
        'security/access_rule.xml',

        'views/notification_views.xml',

        'views/menu_item.xml',
    ],
    "assets": {
        "web.assets_qweb": [
            'x_notify/static/src/components/**/*',
            'x_notify/static/src/xml/**/*',
        ],
        "web.assets_backend": [
            'x_notify/static/src/components/**/*',
            'x_notify/static/src/js/**/*'
        ],
    },
    'application': True,
    'license': 'LGPL-3',
}
