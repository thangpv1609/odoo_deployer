from odoo import fields, models, api
import json


class Users(models.Model):
    _inherit = "res.users"

    def create_notify_with_action(self, content, action):
        res = self.env['notify'].create({
            'content': content,
            'receiver_id': self.id,
            'action': json.dumps(action),
        })
        if res:
            self.env['bus.bus']._sendone(f'notify_{self.id}', 'create', {})

    def create_notify_with_link(self, content, link):
        res = self.env['notify'].create({
            'content': content,
            'receiver_id': self.id,
            'link': link,
        })
        if res:
            self.env['bus.bus']._sendone(f'notify_{self.id}', 'create', {})
