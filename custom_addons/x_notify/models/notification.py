from odoo import fields, models, api


class Notify(models.Model):
    _name = "notify"
    _description = "Thông báo - vns"
    _order = "id desc"

    name = fields.Text(string="Tiêu đề", default="Thông báo hệ thống")
    content = fields.Text(string="Nội dung")
    is_read = fields.Boolean(string="Đã đọc ?", default=False)
    link = fields.Text(string="Liên kết chuyển hướng")
    receiver_id = fields.Many2one('res.users', string="Người nhận")
    target = fields.Selection([('self', 'Tại trang tab hiện tại'), ('new', 'Mở trên tab mới')], string="Cách thức mở",
                              default='new')
    action = fields.Text(string='Hành động')

    def compute_count_unread(self):
        return self.env['notify'].search_count([('is_read', '=', False), ('receiver_id', '=', self.env.user.id)])
